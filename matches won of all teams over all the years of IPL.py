import mainfile as m
import csv
import matplotlib.pyplot as plot


# with open('/home/dev/Downloads/ipl/matches.csv','r') as read:
def calc_matches_won():
    iplfile=csv.DictReader(m.file1fropen())
    yeardict={}
    winnerdict={}
    for row in iplfile:
       filedata=dict(row)
       year=filedata['season']
       winner=filedata['winner']
       winner1 = list(filter(lambda x:x!=" ", winner))
       if winner1:
           if winner not in winnerdict:
               winnerdict[winner]=0
           if year in yeardict:
               if winner in yeardict[year]:
                   yeardict[year][winner]+=1
               else:
                   yeardict[year][winner]=0
           else:
               yeardict[year]={winner:0}
    plotting(yeardict,winnerdict)


def plotting(yeardict,winnerdict):
    color=['red','green','blue','black','yellow','orange','purple','darkgray','khaki','lawngreen','teal','crimson','blueviolet','violet']
    plot.rcParams['figure.figsize'] = (20, 8)
    margins_value = {"left": .04, "bottom": 0.065, "right": .75, "top": .95}
    plot.subplots_adjust(**margins_value)
    for year in sorted(yeardict):
        y_axis=0
        m=0
        for team in yeardict[year]:
            plot.bar(year,yeardict[year][team],bottom=y_axis,color=color[m])
            y_axis+=yeardict[year][team]
            m+=1


    team_list=list(winnerdict.keys())
    plot.xlabel("Years")
    plot.ylabel("Mtches")
    plot.title("Matches won of teams over all the years in IPL")
    plot.legend(sorted(team_list),framealpha=0.1,loc=2,bbox_to_anchor=(1.05,1))

    plot.show()



calc_matches_won()