import mainfile as m
import plotbar


def main_initialization():
    yearid = list()
    file1=m.file1open()
    teamname_and_extrascore = {}

    extracting_data(file1, teamname_and_extrascore, yearid)


def extracting_data(file1,teamname_and_extrascore,yearid):



    for row in range(1,len(file1)-1):
        temp=file1[row].split(",")
        if temp[1]==str(2016):
            yearid.append(temp[0]) #assigning yearid
            teamname_and_extrascore[temp[4]]=0 #initializing team score with 0
    calc_extra_runs(yearid, teamname_and_extrascore)

def  calc_extra_runs(yearid,teamname_and_extrascore):

    file2 = m.file2open()

    for row in range(1,len(file2)-1):
        temp=file2[row].split(",")

        if temp[0] in yearid:

            if (temp[16])>str(0):
                teamname_and_extrascore[temp[3]] += int(temp[16]) #sum of total  of score of each team

    sort_plot(teamname_and_extrascore)

def sort_plot(teamname_and_extrascore):

    # x=[]
    # y=[]
    # for k,v in sorted(teamname_and_extrascore.items()):
    #     x.append(k)
    #     y.append(v)

    t_sort = sorted(zip(teamname_and_extrascore.values(), teamname_and_extrascore.keys()))
    x = []
    y = []

    for i in t_sort:
        x.append(i[1])
        y.append(i[0])

    plotbar.pltbar(x, y, "Team", "Extra Score", "Extra runs conceded per team", "green")  # plotting



main_initialization()
