import numpy as np
from matplotlib import pyplot as plot


def pltbar(x,y,x_label1,y_label1,title1,colour="blue"):
    plot.rcParams['figure.figsize'] = (20, 8)
    margins_value = {"left": .04, "bottom": 0.22, "right": .95, "top": .95}
    plot.subplots_adjust(**margins_value)
    plot.bar(x, y,color=colour)
    plot.xticks(x, rotation='vertical')
    plot.xlabel(x_label1)
    plot.ylabel(y_label1)
    plot.title(title1)
    plot.show()
    return