import mainfile as m
import plotbar as plot

def main_initialize():
    iplfile=m.file1open()
    no_match={}
    extracting_data(iplfile,no_match)

def extracting_data(iplfile,no_match):
    year=[]
    for i in range(1,len(iplfile)-1):
        temp=iplfile[i].split(",") #splitting each column by comma
        year.append(temp[1]) #storing column number 1 to year
        no_match[temp[1]]=0



    for i in range(1,len(iplfile)-1):
        temp = iplfile[i].split(",")
        if temp[1] in year:
            no_match[temp[1]] += 1
    sorting_plotting(no_match) #calling function


def sorting_plotting(no_match):
    yearlist=[]
    matchlist=[]

    t_sort=sorted(zip(no_match.keys(),no_match.values())) #sorting based on no_match keys
    for i in t_sort:
        yearlist.append(i[0])
        matchlist.append(i[1])

    # for i in sorted(no_match.keys()):
    #     yearlist.append(i)
    #     matchlist.append(no_match[i])

    plot.pltbar(yearlist,matchlist, "Matches played", "Year", "Matches played per year of all the teams in IPL", "orange")  # plotting



main_initialize()