import mainfile as m
import plotbar as plot
import copy
from matplotlib import pyplot as plot

def main_initialize():
    file1=m.file1open()
    team={}
    year = {}
    extracting_data(file1,year,team)



def extracting_data(file1,year,team):

    for i in range(1,len(file1)-1):
        temp=file1[i].split(",") #splitting each column by comma
        year[temp[1]]=int(0)
    for i in range(1, len(file1) - 1):
        temp = file1[i].split(",")  # splitting each column by comma
        if temp[1] in year :
            # print("...")
            if temp[11]>str(0):
                team[temp[4]]=0
        year[temp[1]]:{team}




    win_by_runs(file1, year, team)


def win_by_runs(file1,year,team):
    for i in range(1, len(file1) - 1):
        temp = file1[i].split(",")  # splitting each column by comma
        # year.append(temp[1])
        if temp[1] in year:
            if temp[11] > str(0):
                # print("dkn")
                team[temp[4]]+=1

        # year[temp[i]]: {team}
            year[temp[1]] = copy.deepcopy(team)

    # for k,v in year.items():
    #     print(k,v)
    plotting(year, team)


def plotting(yeardict,winnerdict):

    for year in sorted(yeardict):
        y_axis=0
        for team in yeardict[year]:
            plot.bar(year,yeardict[year][team],bottom=y_axis)
            y_axis+=yeardict[year][team]

    teamlist=list(winnerdict.keys())
    plot.xlabel("Years")
    plot.ylabel("Number of times win by run")
    plot.title("Number of times Teams won matches per year by run in IPL")
    plot.legend(teamlist,framealpha=0.1)

    plot.show()

main_initialize()