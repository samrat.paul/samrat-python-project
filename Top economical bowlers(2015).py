import mainfile as m
import plotbar


def inilization():
    yearid= list()
    bowler_dict= {}
    total_run_dict={}
    no_of_bowl={}
    file1=m.file1open()
    file2=m.file2open()
    teamname_and_extrascore={}

    extracting_data(file1, file2, yearid, teamname_and_extrascore, bowler_dict, total_run_dict, no_of_bowl)#function call
    return

def extracting_data(file1,file2,yearid,teamname_and_extrascore,bowler_dict,total_run_dict,no_of_bowl):
        for row in range(1,len(file1)-1):
            temp=file1[row].split(",")
            if temp[1]==str(2015):
                yearid.append(temp[0]) #assigning year id to yearid
                teamname_and_extrascore[temp[4]]=0 #initializing team score with 0




        for row in range(1,len(file2)):
            temp=file2[row].split(",")
            if temp[0] in yearid:
                bowler_dict[temp[8]]=0
                total_run_dict[temp[8]]=0
                no_of_bowl[temp[8]]=0

        calculate_echonomical_bowlar(file2, yearid, total_run_dict, no_of_bowl, bowler_dict)#function call



def calculate_echonomical_bowlar(file2,yearid,totalrundict,no_of_bowl,bowler_dict):
    for row in range(1, len(file2)):
        temp = file2[row].split(",")
        if temp[0] in yearid:
            totalrundict[temp[8]]+= int(temp[17])
            no_of_bowl[temp[8]]+=1
    for i in bowler_dict.keys():
        bowler_dict[i]=totalrundict[i]/no_of_bowl[i] #calculating echonomical bowler

    sort_plot(bowler_dict)#function call



def sort_plot(bowlerdict):
    t_sort=sorted(zip(bowlerdict.values(),bowlerdict.keys()))
    xaxis=[]
    yaxis=[]

    for i in t_sort:
        xaxis.append(i[1])
        yaxis.append(i[0])

    plotbar.pltbar(xaxis[:10],yaxis[:10],"Score","Bowler","Top economical bowlers of 2015","red")#plotting
    return

inilization()#main function call